# Calendário Lunar

Calendário lunar com os 13 ciclos lunares do ano para acompanhar as fases da lua, sua luminosidade e suas datas.

Visite: https://webarthur.gitlab.io/calendariolunar/

## Como imprimir ou salvar PDF

* Abra o link no navegador: https://webarthur.gitlab.io/calendariolunar/
* Selecione o ano do calendário
* Mande imprimir (Ctrl + P)
  * Para imprimir escolha a impressora
  * Para gerar o PDF mande salvar o arquivo PDF

## Características

* Mostrar as fases da lua conforme seus ciclos e data do ano
* Mostrar em qual fase a lua está conforme a data
* Mostrar luminosidade da lua conforme a data

## TODO

* Marcar a hora de subida e descida da lua
* Marcar dias de eclipse conforme o local
* Melhorar visualização no mobile
