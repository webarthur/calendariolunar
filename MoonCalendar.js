var MoonCalendar = function (target, year) {
  if (!year) {
    year = dt.getFullYear()
  }

  var dt = new Date(`${year}-01-01`)
  var html = ''

  // Search for the fist new moon date (at the end of the last year)
  var moon = SunCalc.getMoonIllumination(dt)
  var lastMoon = { phase: 1 }
  while (lastMoon.phase > moon.phase) {
    lastMoon = moon
    dt.setDate(dt.getDate() - 1)
    moon = SunCalc.getMoonIllumination(dt)
  }

  // Prepare variables to the first moon
  var l = SunCalc.getMoonIllumination(dt)
  console.log(l)
  var luminosity = (l.fraction * 100).toFixed(0) + '%'
  var phase = Math.round(l.phase * 100) / 100
  var fullmoon = false
  var anotherMoon = true
  var light
  var moonNumber = 0

  var today = new Date()
  today = String(today.getDate()).padStart(2, '0') + '/' + String(today.getMonth() + 1).padStart(2, '0') + '/' + today.getFullYear()

  // Print the days of the moon
  html += '<b class="mc-day mc-header" style="padding-top:2px">Dias</b>'
  for (var i = 1; i < 31; i++) {
    html += `
      <span class="mc-day mc-header-day">
        <b>${i}</b>
      </span>
    `
  }

  var finish = false
  while (!finish) {
    if (anotherMoon) {
      // The calendar finish at the moon 13
      if (moonNumber === 13) {
        finish = true
        break
      }

      anotherMoon = false
      moonNumber += 1

      // Print row header
      html += '<hr class="mc-hr">'
      html += `
        <span class="mc-day mc-header">
          <b>LUA</b>
          <span>
            ${String(moonNumber).padStart(2, '0')}
          </span>
        </span>
      `
    }

    // Prepare the day variables
    var d = dt.getDate()
    var m = String(dt.getMonth() + 1).padStart(2, '0')
    var moonPhase = ''
    var l = SunCalc.getMoonIllumination(dt)
    light = l.fraction

    // Check if the date is today
    var isToday = today === `${String(d).padStart(2, '0')}/${m}/${dt.getFullYear()}`

    // New moon
    moonPhase = ''
    var css = ''
    var border = 1
    var specialDate = false
    var newMoonFactor = 0.02 // when the new cycle begins

    // New moon
    // if (phase < .5 && light > 0 && light < .01 ) moonPhase = ''
    // Waxing crescent
    if (phase < 0.5 && light >= newMoonFactor && light < 0.5) {
      moonPhase = 'mc-moon1'
      // css = `right:${(100 - light * 100).toFixed(0)}%`
    }
    // Waxing gibbous
    if (phase < 0.5 && light >= 0.5 && light < 0.98) {
      moonPhase = 'mc-moon3'
      // css = `right:${(100 - light * 100).toFixed(0)}%`
    }
    // Full moon
    if (phase < 0.6 && light >= 0.98 && light <= 1) {
      moonPhase = 'mc-fullmoon'
      fullmoon = true
      if (light > 0.995) {
        border = 2
        specialDate = true
      }
    }
    // Waning gibbous
    if (phase >= 0.5 && light >= 0.5 && light < 0.98) {
      moonPhase = 'mc-moon4'
      // css = `left:${(100 - light * 100).toFixed(0)}%`
    }
    // Waning crescent
    if (phase >= 0.5 && light < 0.5 && light >= newMoonFactor) {
      moonPhase = 'mc-moon6'
      // css = `left:${(100 - light * 100).toFixed(0)}%`
    }
    // if (phase >= .5 && light < newMoonFactor) moonPhase = ''

    // HACK First quarter (.25) = mulinosity 50%
    if (phase < 0.5 && light >= 0.442 && light < 0.559) {
      moonPhase = 'mc-moon2'
      // css = `right:${(100 - light * 100).toFixed(0)}%`
      border = 2
      specialDate = true
    }
    if (phase > 0.5 && light >= 0.442 && light < 0.559) {
      moonPhase = 'mc-moon5'
      // css = `right:${(100 - light * 100).toFixed(0)}%`
      border = 2
      specialDate = true
    }

    // Get the moon luminosity
    var luminosity = (l.fraction * 100).toFixed(1) + '%'

    // Add moon date to print
    html += `
        <span class="mc-day ${isToday ? 'mc-today' : ''}">
          <span class="mc-luminosity">${luminosity}</span>
          <br>
          <span class="mc-moon ${moonPhase}"><span></span></span>
          <span class="mc-date" ${specialDate ? 'style="font-weight:bold"' : ''}>${d}/${m}</span>
        </span>
      `
    // style="${specialDate && 'box-shadow: 0 0 10px'}"

    // Set the next moon cycle
    if (fullmoon && phase >= 0.5 && light < newMoonFactor) {
      fullmoon = false
      anotherMoon = true
    }

    // Update the date and the moon phase
    dt.setTime(dt.getTime() + (1000 * 60 * 60 * 24)) // +1 day
    phase = Math.round(l.phase * 100) / 100
  }

  document.querySelector(target).innerHTML = html
}
